import { assets } from "@/Assets/assets";
import Image from "next/image";
import React from "react";

const BlogTableItem = ({ authorImg, title, author, date,deleteBlog,mongoId }) => {
  let BlogDate;
  try {
    BlogDate = new Date(date);
    if (isNaN(BlogDate.getTime())) {
      throw new Error("Invalid date");
    }
  } catch (error) {
    console.error("Invalid date format: ", date);
    BlogDate = new Date(); // Fallback to the current date
  }

  return (
    <tr className="bg-white border-b">
      <td className="px-6 py-4">{title ? title : "no title"}</td>
      <th
        scope="row"
        className="items-center gap-3 hidden sm:flex px-6 py-4 font-medium text-gray-500 whitespace-nowrap"
      >
        <Image
          width={40}
          height={40}
          src={authorImg ? authorImg : assets.profile_icon}
          alt="Author"
        />      <td className="px-6 py-4">{author ? author : "no author"}</td>

       
      </th>
      
      <td className="px-6 py-4">{BlogDate.toDateString()}</td>
      <td onClick={()=>deleteBlog(mongoId)} className="px-6 py-4 cursor-pointer">x </td>
    </tr>
  );
};

export default BlogTableItem;
