
import { assets, blog_data } from "@/Assets/assets";
import Image from "next/image";
import Link from "next/link";
import React from "react";

const BlogItem = ({id,title,description,image,category}) => {
  return (
    <div  className="max-w-[330px] sm:max-w-[300px] bg-white border border-black hover:shadow-[-7px_7px_0px_#000000]">
     <Link href={`/blogs/${id}`}>
     <Image
      alt=""
        width={400}
        height={400}
        className=" border-b border-black"
        src={image}
      />
     </Link>
      
      <p className="ml-5 mt-5 px-1  inline-block  bg-black  text-white text-sm">
        {category}
      </p>
      <h5 className="ml-5 mb-2 text-lg font-medium tracking-tight text-gray-900">
        {title}
      </h5>
      <p className="ml-5 mb-3 text-sm tracking-tight text-gray-700">
        {description}
      </p>
    <Link href={`/blogs/${id}`}>
    <div className="ml-5 inline-flex items-center py-2 font-semibold text-center gap-2">
        Read more
        <Image  alt="" src={assets.arrow} width={12} />
      </div>
    </Link>
    </div>
  );
};

export default BlogItem;
