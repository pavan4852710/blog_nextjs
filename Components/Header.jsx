'use client'
import { assets } from "@/Assets/assets";
import axios from "axios";
import Image from "next/image";
import React, { useState } from "react";
import { toast } from "react-toastify";

const Header = () => {
  const [email, setEmail] = useState("");
/*
  const onSubmitHandler = async (e) => {
    e.preventDefault();

    if (!email) {
      toast.error("Email is required");
      return;
    }

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      toast.error("Please enter a valid email address");
      return;
    }

    const formData = new FormData(); // FormData object created
    formData.append("email", email);

    try {
      const response = await axios.post('/api/email', formData);
      if (response.data.success) {
        toast.success(response.data.msg);
        setEmail("");
      } else {
        toast.error("ERROR");
      }
    } catch (error) {
      toast.error("An error occurred. Please try again.");
    }
  };
*/
  return (
    <div className="py-5 px-5 md:px-12 lg:px-28">
      <div className="flex justify-between items-center">
        <Image
          src={assets.logo}
          width={180}
          alt="Logo"
          className="w-[130px] sm:w-auto"
        />
        <button className="flex items-center gap-2 font-medium py-1 px-3 sm:py-3 sm:px-6 border border-solid border-black shadow-[-7px_7px_0px_000000]">
          Get Started 
        </button>
      </div>

      <div className="text-center my-8 items-center">
        <h1 className="text-3xl sm:text-5xl font-medium">Latest Blogs</h1>
        <p className="items-center mt-10 mx-auto max-w-full sm:max-w-[740px] text-xs sm:text-base">
          Get Latest Information and News from trusted people.
        </p>
        <form
         // onSubmit={onSubmitHandler}
          className="flex justify-between mx-auto max-w-[500px] border scale-75 sm:scale-100"
        >
          <input
            //onChange={(e) => setEmail(e.target.value)}
          //  value={email}
            type="email"
            placeholder="Enter your email"
            aria-label="Email"
            className="pl-4 outline-none"
          />
          <button
            type="submit"
            className="border-1 border-black text-black py-4 px-4 sm:px-8 active:bg-gray-600 active:text-white"
            aria-label="Subscribe"
          >
            Subscribe
          </button>
        </form>
      </div>
    </div>
  );
};

export default Header;
