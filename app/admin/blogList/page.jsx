"use client";
import BlogTableItem from "@/Components/AdminComponents/BlogTableItem";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";

const Page = () => {
  const [blogs, setBlogs] = useState([]);

  //get blog
  const fetchBlogs = async () => {
    try {
      const response = await axios.get("/api/blog");
      setBlogs(response.data.blogs);
      console.log(response.data.blogs);
    } catch (error) {
      console.error("Error fetching blogs:", error);
    }
  };

  //delete blog
  const deleteBlog = async (mongoId) => {

      const response = await axios.delete("/api/blog", {
        params: {
          id: mongoId,
        },
      });
      toast.success(response.data.msg);
      alert("delete blog successsfully")
      fetchBlogs();
   
  };

  useEffect(() => {
    fetchBlogs();
  }, []);

  return (
    <div className="flex-1 pt-5 px-5 sm:pt-12">
      <h1>All blogs</h1>
      <div className="relative h-[80vh] max-w-[850px] overflow-x-auto mt-4 border border-gray-700">
        <table className="w-full text-sm text-gray-500">
          <thead className="text-sm text-gray-700 text-left uppercase bg-gray-100">
            <tr>
              <th scope="col" className="px-6 py-3">
                Blog Title
              </th>
              <th scope="col" className="hidden sm:block px-6 py-3">
                Author Name
              </th>

              <th scope="col" className="px-6 py-3">
                Blog Date
              </th>
              <th scope="col" className="px-6 py-3">
                Action
              </th>
            </tr>
          </thead>
          <tbody>
            {blogs.map((item, index) => (
              <BlogTableItem
                deleteBlog={deleteBlog}
                mongoId={item._id}
                key={index}
                authorImg={item.authorImg}
                title={item.title}
                author={item.author}
                date={item.date} // Corrected from item.data to item.date
              />
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Page;
